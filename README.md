# mobfirst-vtex-register-pod

[![CI Status](http://img.shields.io/travis/Diego Merks/mobfirst-vtex-register-pod.svg?style=flat)](https://travis-ci.org/Diego Merks/mobfirst-vtex-register-pod)
[![Version](https://img.shields.io/cocoapods/v/mobfirst-vtex-register-pod.svg?style=flat)](http://cocoapods.org/pods/mobfirst-vtex-register-pod)
[![License](https://img.shields.io/cocoapods/l/mobfirst-vtex-register-pod.svg?style=flat)](http://cocoapods.org/pods/mobfirst-vtex-register-pod)
[![Platform](https://img.shields.io/cocoapods/p/mobfirst-vtex-register-pod.svg?style=flat)](http://cocoapods.org/pods/mobfirst-vtex-register-pod)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

mobfirst-vtex-register-pod is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "mobfirst-vtex-register-pod"
```

## Author

Diego Merks, merks@mobfirst.com

## License

mobfirst-vtex-register-pod is available under the MIT license. See the LICENSE file for more info.
