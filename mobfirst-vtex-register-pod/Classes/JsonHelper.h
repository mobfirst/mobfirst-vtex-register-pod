//
//  JsonHelper.h
//  VtexRegisterTest
//
//  Created by Teste Eclipse 2 on 10/07/17.
//  Copyright © 2017 Teste Eclipse 2. All rights reserved.
//

#ifndef JsonHelper_h
#define JsonHelper_h

@interface JsonHelper : NSObject

+ (NSString *) getDocumentField:(NSString *) field fromSearchResult:(NSString *) searchResult;

@end

#endif /* JsonHelper_h */
