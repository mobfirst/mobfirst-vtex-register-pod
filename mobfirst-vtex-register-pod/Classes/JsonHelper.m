//
//  JsonHelper.m
//  VtexRegisterTest
//
//  Created by Teste Eclipse 2 on 10/07/17.
//  Copyright © 2017 Teste Eclipse 2. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JsonHelper.h"

@implementation JsonHelper

+ (NSString *) getDocumentField:(NSString *) field fromSearchResult:(NSString *) searchResult {
    
    NSError *error;
    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:[searchResult dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
    
    if(error) return nil;
    
    for(NSDictionary *dictionary in jsonArray) {
        if(!dictionary) continue;
        
        NSString *documentId = [dictionary objectForKey:field];
        if(!documentId) continue;
        
        if([documentId isKindOfClass:[NSNull class]]) return nil;
        else return documentId;
    }
    
    return nil;
}

@end
