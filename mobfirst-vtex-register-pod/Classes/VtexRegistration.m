//
//  VtexRegistration.m
//  VtexRegisterTest
//
//  Created by Teste Eclipse 2 on 10/07/17.
//  Copyright © 2017 Teste Eclipse 2. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VtexRegistration.h"
#import "RegisterHttp.h"
#import "JsonHelper.h"

#define VTEX_MASTERDATA_CL_SEARCH_URL @"https://api.vtexcrm.com.br/%@/dataentities/CL/search?userId=%@&_fields=id"
#define VTEX_MASTERDATA_CL_EMAIL_SEARCH_URL @"https://api.vtexcrm.com.br/%@/dataentities/CL/search?email=%@&_fields=id,userId"
#define VTEX_MASTERDATA_CA_SEARCH_URL @"https://api.vtexcrm.com.br/%@/dataentities/CA/search?accessKey=%@&_fields=id,group,accessKey,sc"
#define VTEX_MASTERDATA_POST_URL @"https://api.vtexcrm.com.br/%@/dataentities/%@/documents"
#define VTEX_MASTERDATA_PATCH_URL @"https://api.vtexcrm.com.br/%@/dataentities/%@/documents/%@"

@implementation VtexRegistration

- (id) initWithAccountName:(NSString *) accountName appKey:(NSString *) appKey appToken:(NSString *) appToken {
    
    self = [super init];
    
    if(self) {
        
        self.accountName = accountName;
        self.appKey = appKey;
        self.appToken = appToken;
    }
    
    return self;
}

- (void) registerOrUpdateUserWithId:(NSString *) userId fields:(NSDictionary *) fields andCompletionHandler:(void(^)(bool success, NSData *data)) completionHandler {
    
    NSMutableDictionary *headers = [[NSMutableDictionary alloc] init];
    
    [headers setValue:@"application/json" forKey:@"Content-Type"];
    [headers setValue:[self appKey] forKey:@"x-vtex-api-appKey"];
    [headers setValue:[self appToken] forKey:@"x-vtex-api-appToken"];
    
    if(userId) {
        
        NSURL *searchUrl = [NSURL URLWithString:[NSString stringWithFormat:VTEX_MASTERDATA_CL_SEARCH_URL, [self accountName], userId]];
        [RegisterHttp doGetFromUrl:searchUrl withHeaders:headers andCompletionHandler:^(NSData *data, NSURLResponse *response) {
            
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            if([httpResponse statusCode] == 200) {
                
                NSString *documentId = [JsonHelper getDocumentField: @"id" fromSearchResult:[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]];
                if(documentId) {
                    
                    NSURL *patchUrl = [NSURL URLWithString:[NSString stringWithFormat:VTEX_MASTERDATA_PATCH_URL, [self accountName], @"CL", documentId]];
                    [RegisterHttp doPatchToUrl:patchUrl withHeaders:headers body:fields andCompletionHandler:^(NSData *data, NSURLResponse *response) {
                        
                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                        completionHandler([httpResponse statusCode] == 204, data);
                    }];
                }
                else {
                    
                    NSURL *postUrl = [NSURL URLWithString:[NSString stringWithFormat:VTEX_MASTERDATA_POST_URL, [self accountName], @"CL"]];
                    [fields setValue:userId forKey:@"userId"];
                    
                    [RegisterHttp doPostToUrl:postUrl withHeaders:headers body:fields andCompletionHandler:^(NSData *data, NSURLResponse *response) {
                        
                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                        completionHandler([httpResponse statusCode] == 201, data);
                    }];
                }
            }
            else {
                completionHandler(false, data);
            }
        }];
    }
    else {
        
        NSURL *postUrl = [NSURL URLWithString:[NSString stringWithFormat:VTEX_MASTERDATA_POST_URL, [self accountName], @"CL"]];
        [RegisterHttp doPostToUrl:postUrl withHeaders:headers body:fields andCompletionHandler:^(NSData *data, NSURLResponse *response) {
            
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            completionHandler([httpResponse statusCode] == 201, data);
        }];
    }
}

- (void) checkIfEmailExists:(NSString *) email withCompletionHandler:(void(^)(bool success, bool exists, NSString * userId)) completionHandler {
    
    if(email) {
        
        NSMutableDictionary *headers = [[NSMutableDictionary alloc] init];
        NSURL *searchUrl = [NSURL URLWithString:[NSString stringWithFormat:VTEX_MASTERDATA_CL_EMAIL_SEARCH_URL, [self accountName], email]];
        
        [headers setValue:@"application/json" forKey:@"Content-Type"];
        [headers setValue:[self appKey] forKey:@"x-vtex-api-appKey"];
        [headers setValue:[self appToken] forKey:@"x-vtex-api-appToken"];
        
        [RegisterHttp doGetFromUrl:searchUrl withHeaders:headers andCompletionHandler:^(NSData *data, NSURLResponse *response) {
            
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            if([httpResponse statusCode] == 200) {
                
                NSString *response = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                NSString *userId = [JsonHelper getDocumentField:@"userId" fromSearchResult:response];
                NSString *documentId = [JsonHelper getDocumentField:@"id" fromSearchResult:response];
                
                if(documentId) completionHandler(YES, YES, userId);
                else completionHandler(YES, NO, nil);
            }
            else completionHandler(NO, NO, nil);
        }];
    }
    else completionHandler(NO, NO, nil);
}

- (void) registerOrUpdateHomeToGoUserWithUserId:(NSString *) userId firstName:(NSString *) firstName lastName:(NSString *) lastName email:(NSString *) email document:(NSString *) document phone:(NSString *) phone accessKey:(NSString *) accessKey andCompletionHandler:(void(^)(bool success, bool validAccessKey, NSData *data)) completionHandler {
    
    if(firstName && lastName && email && document && phone && accessKey) {
        
        __block NSString *bFirstName = firstName;
        __block NSString *bLastName = lastName;
        __block NSString *bEmail = email;
        __block NSString *bDocument = document;
        __block NSString *bPhone = phone;
        
        [self getHomeToGoAccessKey:accessKey withCompletionHandler:^(bool success, NSString *accessKeyId) {
            
            if(success && accessKeyId) {
                
                NSDictionary *fields = [[NSDictionary alloc] initWithObjectsAndKeys:
                                        bFirstName, @"firstName",
                                        bLastName, @"lastName",
                                        bEmail, @"email",
                                        bDocument, @"document",
                                        bPhone, @"phone",
                                        accessKeyId, @"accessKey", nil];
                
                [self registerOrUpdateUserWithId:userId fields:fields andCompletionHandler:^(bool success, NSData *data) {
                    completionHandler(success, YES, data);
                }];
            }
            else completionHandler(NO, NO, nil);
        }];
    }
    else completionHandler(NO, NO, nil);
}

- (void) getHomeToGoAccessKey:(NSString *) accessKey withCompletionHandler:(void(^)(bool success, NSString *accessKeyId)) completionHandler {
    
    NSMutableDictionary *headers = [[NSMutableDictionary alloc] init];
    NSURL *searchUrl = [NSURL URLWithString:[NSString stringWithFormat:VTEX_MASTERDATA_CA_SEARCH_URL, [self accountName], accessKey]];
    
    [headers setValue:@"application/json" forKey:@"Content-Type"];
    [headers setValue:[self appKey] forKey:@"x-vtex-api-appKey"];
    [headers setValue:[self appToken] forKey:@"x-vtex-api-appToken"];
    
    [RegisterHttp doGetFromUrl:searchUrl withHeaders:headers andCompletionHandler:^(NSData *data, NSURLResponse *response) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        if(data && [httpResponse statusCode] == 200) {
            
            NSString *accessKeyId = [JsonHelper getDocumentField: @"id" fromSearchResult:[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]];
            
            if(accessKeyId) completionHandler(YES, accessKeyId);
            else completionHandler(NO, nil);
        }
        else completionHandler(NO, nil);
    }];
}

- (void) requestAccessKeyWithFirstName:(NSString *) firstName lastName:(NSString *) lastName email:(NSString *) email document:(NSString *) document andCompletionHandler:(void(^)(bool success, NSData *data)) completionHandler {
    
    if(firstName && lastName && email && document) {
        
        NSMutableDictionary *headers = [[NSMutableDictionary alloc] init];
        NSURL *postUrl = [NSURL URLWithString:[NSString stringWithFormat:VTEX_MASTERDATA_POST_URL, [self accountName], @"NK"]];
        
        [headers setValue:@"application/json" forKey:@"Content-Type"];
        [headers setValue:[self appKey] forKey:@"x-vtex-api-appKey"];
        [headers setValue:[self appToken] forKey:@"x-vtex-api-appToken"];
        
        NSDictionary *fields = [[NSDictionary alloc] initWithObjectsAndKeys:
                                firstName, @"firstName",
                                lastName, @"lastName",
                                email, @"email",
                                document, @"document", nil];
        
        [RegisterHttp doPostToUrl:postUrl withHeaders:headers body:fields andCompletionHandler:^(NSData *data, NSURLResponse *response) {
            
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            completionHandler([httpResponse statusCode] == 201, data);
        }];
    }
    else completionHandler(NO, nil);
}

@end
