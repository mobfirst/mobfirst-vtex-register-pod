//
//  VtexRegistration.h
//  VtexRegisterTest
//
//  Created by Teste Eclipse 2 on 10/07/17.
//  Copyright © 2017 Teste Eclipse 2. All rights reserved.
//

#ifndef VtexRegistration_h
#define VtexRegistration_h

@interface VtexRegistration : NSObject

@property (nonatomic, strong) NSString *accountName;
@property (nonatomic, strong) NSString *acronym;
@property (nonatomic, strong) NSString *appKey;
@property (nonatomic, strong) NSString *appToken;

- (id) initWithAccountName:(NSString *) accountName appKey:(NSString *) appKey appToken:(NSString *) appToken;
- (void) registerOrUpdateUserWithId:(NSString *) userId fields:(NSDictionary *) fields andCompletionHandler:(void(^)(bool success, NSData *data)) completionHandler;
- (void) registerOrUpdateHomeToGoUserWithUserId:(NSString *) userId firstName:(NSString *) firstName lastName:(NSString *) lastName email:(NSString *) email document:(NSString *) document phone:(NSString *) phone accessKey:(NSString *) accessKey andCompletionHandler:(void(^)(bool success, bool validAccessKey, NSData *data)) completionHandler;
- (void) checkIfEmailExists:(NSString *) email withCompletionHandler:(void(^)(bool success, bool exists, NSString * userId)) completionHandler;
- (void) getHomeToGoAccessKey:(NSString *) accessKey withCompletionHandler:(void(^)(bool success, NSString *accessKeyId)) completionHandler;
- (void) requestAccessKeyWithFirstName:(NSString *) firstName lastName:(NSString *) lastName email:(NSString *) email document:(NSString *) document andCompletionHandler:(void(^)(bool success, NSData *data)) completionHandler;

@end

#endif /* VtexRegistration_h */
