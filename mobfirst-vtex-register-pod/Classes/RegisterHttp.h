//
//  Http.h
//  VtexRegisterTest
//
//  Created by Teste Eclipse 2 on 10/07/17.
//  Copyright © 2017 Teste Eclipse 2. All rights reserved.
//

#ifndef Http_h
#define Http_h

@interface RegisterHttp : NSObject

+ (void) doGetFromUrl:(NSURL *) url withHeaders:(NSDictionary *) headers andCompletionHandler:(void(^)(NSData *data, NSURLResponse *response)) completionHandler;
+ (void) doPostToUrl:(NSURL *) url withHeaders:(NSDictionary *) headers body:(NSDictionary *) body andCompletionHandler:(void(^)(NSData *data, NSURLResponse *response)) completionHandler;
+ (void) doPatchToUrl:(NSURL *) url withHeaders:(NSDictionary *) headers body:(NSDictionary *) body andCompletionHandler:(void(^)(NSData *data, NSURLResponse *response)) completionHandler;

@end

#endif /* Http_h */
