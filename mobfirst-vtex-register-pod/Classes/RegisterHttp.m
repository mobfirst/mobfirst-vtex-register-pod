//
//  Http.m
//  VtexRegisterTest
//
//  Created by Teste Eclipse 2 on 10/07/17.
//  Copyright © 2017 Teste Eclipse 2. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RegisterHttp.h"

#define TIMEOUT_INTERVAL 5.0

@implementation RegisterHttp

+ (void) doGetFromUrl:(NSURL *) url withHeaders:(NSDictionary *) headers andCompletionHandler:(void(^)(NSData *data, NSURLResponse *response)) completionHandler {
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIMEOUT_INTERVAL];
    [request setHTTPMethod:@"GET"];
    
    for(NSString *key in [headers allKeys]) {
        [request setValue:[headers objectForKey:key] forHTTPHeaderField:key];
    }
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        completionHandler(data, response);
    }] resume];
}

+ (void) doPostToUrl:(NSURL *) url withHeaders:(NSDictionary *) headers body:(NSDictionary *) body andCompletionHandler:(void(^)(NSData *data, NSURLResponse *response)) completionHandler {
    
    NSError *error;
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIMEOUT_INTERVAL];
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:body options:NSJSONWritingPrettyPrinted error:&error];
    
    if(!jsonData) {
        
        completionHandler(nil, nil);
        return;
    }
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody: jsonData];
    
    for(NSString *key in [headers allKeys]) {
        [request setValue:[headers objectForKey:key] forHTTPHeaderField:key];
    }
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        completionHandler(data, response);
    }] resume];
}

+ (void) doPatchToUrl:(NSURL *) url withHeaders:(NSDictionary *) headers body:(NSDictionary *) body andCompletionHandler:(void(^)(NSData *data, NSURLResponse *response)) completionHandler {
    
    NSError *error;
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIMEOUT_INTERVAL];
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:body options:NSJSONWritingPrettyPrinted error:&error];
    
    if(!jsonData) {
        
        completionHandler(nil, nil);
        return;
    }
    
    [request setHTTPMethod:@"PATCH"];
    [request setHTTPBody: jsonData];
    
    for(NSString *key in [headers allKeys]) {
        [request setValue:[headers objectForKey:key] forHTTPHeaderField:key];
    }
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        completionHandler(data, response);
    }] resume];
}

@end
